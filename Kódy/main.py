from machine import ADC,Pin,PWM,I2C
import utime
import math
from lcd_api import LcdApi
from pico_i2c_lcd import I2cLcd


I2C_ADDR     = 39
I2C_NUM_ROWS = 2
I2C_NUM_COLS = 16

i2c = I2C(0, sda=machine.Pin(0), scl=machine.Pin(1), freq=400000)
lcd = I2cLcd(i2c, I2C_ADDR, I2C_NUM_ROWS, I2C_NUM_COLS)

thermistor1 = ADC(0)
thermistor2 = ADC(1)
Vin = 3.3
Ro = 10000


#steinhart constants
A = 0.001129148
B = 0.000234125
C = 0.0000000876741

buzzer = Pin(18,Pin.OUT)
buzzer_pwm = PWM(buzzer)
buzzer_pwm.freq(750)

led_pin = Pin(15, Pin.IN, Pin.PULL_DOWN)
led = Pin("LED", Pin.OUT)
servo_pin = machine.Pin(16)
servo_pwm = machine.PWM(servo_pin)
servo_pwm.freq(50)

pocet = 0 


conversion_factor = (3.3/65535)

def buzz(duration):
    buzzer_pwm.duty_u16(32768)
    utime.sleep(duration)
    buzzer_pwm.duty_u16(0)
    
def customcharacter():
    
  #character      
  lcd.custom_char(0, bytearray([
  0x07,
  0x05,
  0x07,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00
        
        ]))    

      
while True:
    
    if led_pin.value() == 0:
        pocet +=1
        led.value(1)
        pozice_serva = 110 
        servo_pwm.duty_u16(int((500 * pozice_serva / 9) + 1500))
    
        utime.sleep(1)
    else:
        led.value(0)
        pozice_serva = 0  # Upravte podle potřeby
        servo_pwm.duty_u16(int((500 * pozice_serva / 9) + 1500))
    
    
    voltage1 = thermistor1.read_u16()*conversion_factor
    Rt1 = ((voltage1 * Ro)/(Vin - voltage1))
    
    TempK1 = 1 / (A + (B * math.log(Rt1)) + C * math.pow(math.log(Rt1), 3))
    TempC1 = TempK1 - 283.15
    #rounded_tempC = round(TempC, 2)
    
    voltage2 = thermistor2.read_u16()*conversion_factor
    Rt2 = ((voltage2 * Ro)/(Vin - voltage2))
    
    TempK2 = 1 / (A + (B * math.log(Rt2)) + C * math.pow(math.log(Rt2), 3))
    TempC2 = TempK2 - 283.15
    print(TempC2)
    TempC_final = (TempC1 + TempC2)/2
    rounded_tempC = round(TempC_final, 2)
    
    customcharacter()
    lcd.move_to(0,0)
    lcd.putstr("TEPLOTA:" + str(rounded_tempC))
    
    lcd.move_to(13,0)
    lcd.putchar(chr(0))
    
    lcd.move_to(14,0)
    lcd.putstr("C")
    
    lcd.move_to(0,1)
    lcd.putstr("POCET :" + str(pocet))
    
    if TempC1 > 50 or TempC2 > 50:
        buzz(2)
    utime.sleep(1) 
             
